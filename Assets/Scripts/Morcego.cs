﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Morcego : MonoBehaviour {

	public float xDir = 1;
	public float deathAnimationTime = 1;
	public float deathAnimationDeltatime = 0;
	public bool active = false;
	public bool playingDeathAnimation = false;
	public bool ready = true;

	MorcegoManager manager;

	public void SetManager(MorcegoManager _manager){
		manager = _manager;
	}

	public void KillMe(){
		manager.KillMorcego (gameObject);
	}

	public void Recycle(){
		transform.position = new Vector2 (2000, 2000);
		active = false;
		ready = true;
		playingDeathAnimation = false;
		deathAnimationDeltatime = 0;
		GetComponent<Collider2D> ().enabled = false;
	}

}
