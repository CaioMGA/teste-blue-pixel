﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	Rigidbody2D rb2d;

	bool ready = false;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		Recycle ();
	}
	
	void Update () {
		
	}

	public void Recycle(){
		transform.position = new Vector2 (2000, 2000);
		ready = true;
	}

	public void Shoot(float force){
		rb2d.velocity = Vector2.zero;
		rb2d.AddRelativeForce(new Vector2(0f, force));
		ready = false;

	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.CompareTag("Enemy")){
			other.GetComponent<Morcego> ().KillMe ();
		}
		Recycle ();
	}

	public bool IsReady(){
		return ready;
	}
}
