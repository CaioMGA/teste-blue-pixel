﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {
	public GameObject pauseMenu;
	public Text batsDefeated;
	public Text batsRunAway;
	public Text score;

	int batsDefeatedCount = 0;
	int batsRunAwayCount = 0;
	int scoreCount = 0;

	Animator batsDefeatedAnim;
	Animator batsRunAwayAnim;
	Animator scoreAnim;

	MenuScript menuScript;

	void Start(){
		menuScript = GameObject.FindObjectOfType<MenuScript> ();

		batsDefeatedAnim = batsDefeated.GetComponent<Animator>();
		batsRunAwayAnim = batsRunAway.GetComponent<Animator>();
		scoreAnim = score.GetComponent<Animator>();

		updateHUD ();
	}

	void Update () {
		if(Input.GetKeyUp(KeyCode.Escape)){
			if(Time.timeScale == 1){
				menuScript.pause ();
				menuScript.toggle (pauseMenu);
			}
		}
	}

	public void updateHUD(){
		batsDefeated.text = "Bats Defeated: " + batsDefeatedCount;
		batsRunAway.text = "Bats Run Away: " + batsRunAwayCount;
		score.text = "Score: " + scoreCount;
	}

	public void UpdateScore(){
		scoreCount += 10;
		scoreAnim.SetTrigger ("UPDATE");

	}

	public void UpdateBatsDefeated(){
		batsDefeatedCount += 1;
		batsDefeatedAnim.SetTrigger ("UPDATE");
	}

	public void UpdateBatsRunAway(){
		batsRunAwayCount += 1;
		batsRunAwayAnim.SetTrigger ("UPDATE");
	}
}
