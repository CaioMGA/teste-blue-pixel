﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorcegoManager : MonoBehaviour {
	public GameObject morcegoPrefab;
	public float minX = -32;
	public float maxX = 32;
	public float minY = -35;
	public float hSpeed = 4;
	public float vSpeed = 1;
	public AudioClip morcegoMorte;
	public AudioClip morcegoAway;

	float spawnDeltatime = 0f;
	float spawnTime = 2f;
	int batCount = 0;

	List<GameObject> morcegos = new List<GameObject>();
	AudioSource audioSource;

	GameLogic gl;

	void Start(){
		audioSource = GetComponent<AudioSource> ();
		gl = GameObject.FindObjectOfType<GameLogic> ();
		CreateMorcegos (5);
	}

	public void newMorcego(){
		GameObject bat = Instantiate (morcegoPrefab);
		bat.GetComponent<Morcego>().SetManager (GetComponent<MorcegoManager>());
		morcegos.Add(bat);
		batCount += 1;
		bat.GetComponent<Morcego> ().Recycle ();
	}

	public void updatePositions(){
		for(int i = 0; i < batCount; i++){
			if(morcegos[i].GetComponent<Morcego>().active){
				morcegos[i].transform.position = new Vector2 (
					morcegos[i].transform.position.x + hSpeed * morcegos[i].GetComponent<Morcego> ().xDir,
					morcegos[i].transform.position.y - vSpeed
				);
				if(morcegos[i].transform.position.x >= maxX || morcegos[i].transform.position.x < minX){
					morcegos[i].GetComponent<Morcego> ().xDir *= -1;
				}
				if(morcegos[i].transform.position.y < minY){
					MorcegoGotAway (morcegos[i]);
				}
			}
		}
	}

	void watchAnimationStates(){
		for (int i = 0; i < batCount; i++) {
			Morcego m = morcegos [i].GetComponent<Morcego> ();
			if(!m.active && m.playingDeathAnimation){
				m.deathAnimationDeltatime += Time.deltaTime;
				if(m.deathAnimationDeltatime >= m.deathAnimationTime){
					m.Recycle ();
				}
			}
		}
	}

	public void KillMorcego(GameObject bat){
		bat.GetComponentInChildren<Animator> ().SetTrigger ("DEATH");
		bat.GetComponent<Morcego> ().active = false;
		bat.GetComponent<Morcego> ().playingDeathAnimation = true;
		bat.GetComponent<Collider2D> ().enabled = false;

		gl.UpdateBatsDefeated ();
		gl.UpdateScore ();
		gl.updateHUD ();

		audioSource.PlayOneShot (morcegoMorte);
	}

	public void MorcegoGotAway(GameObject bat){
		bat.GetComponent<Morcego> ().Recycle ();
		gl.UpdateBatsRunAway ();
		gl.updateHUD ();
		audioSource.PlayOneShot (morcegoAway);
	}

	void Update(){
		if (Time.timeScale == 1) { //se não estiver pausado
			if(spawnDeltatime > spawnTime){
				DeployMorcego ();
				spawnDeltatime = 0;
			} else {
				spawnDeltatime += Time.deltaTime;
			}
			updatePositions ();
			watchAnimationStates ();
		}
	}

	void CreateMorcegos (int morcegoCount){
		for(int i = 0; i < morcegoCount; i++){
			newMorcego ();
		}
	}

	GameObject GetMorcego(){
		for(int i = 0; i < batCount; i++){
			if(morcegos[i].GetComponent<Morcego>().ready){
				return morcegos[i];
			}
		}
		return null;
	}

	void DeployMorcego(){
		GameObject bat = GetMorcego ();
		if(bat == null){
			return;
		}
		bat.transform.position = new Vector2 (Random.Range (minX, maxX), 32);

		//Sorteia a direção inicial
		float foo = Random.Range (0, 2);
		if(foo < 1){
			bat.GetComponent<Morcego> ().xDir = 1;
		} else {
			bat.GetComponent<Morcego> ().xDir = -1;
		}
		bat.GetComponent<Morcego> ().active = true;
		bat.GetComponent<Morcego> ().ready = false;
		bat.GetComponent<Collider2D> ().enabled = true;
		bat.GetComponentInChildren<Animator> ().SetTrigger ("IDLE");
	}
}
