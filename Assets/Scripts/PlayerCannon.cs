﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCannon : MonoBehaviour {
	public GameObject bulletPrefab;
	public float bulletSpeed = 10;
	public AudioClip shotFx;
	public Animator cannonAnim;
	public Animator smokeAnim;

	float angleIncrement = 0.5f;
	float angle = 0f;
	float minAngle = -60;
	float maxAngle = 60;

	AudioSource audioSource;
	Transform cannonPivot;
	Transform bulletDeploy;
	GameObject onScreenBullet;

	BulletScript onScreenBulletSript;

	//flags
	bool canShoot = true;

	void Start () {
		audioSource = GetComponent<AudioSource>();
		cannonPivot = transform.GetChild (0);
		bulletDeploy = cannonPivot.GetChild (0);
		CreateBullet ();
	}

	void Update(){
		if(Time.timeScale == 1){ // Se não estiver pausado
			angle += angleIncrement;
			if(angle > maxAngle || angle < minAngle){
				angleIncrement *= -1;
			}
			cannonPivot.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);

			if(Input.GetKeyUp(KeyCode.Space)){
				if(canShoot){
					Shoot ();
				}
			}

			if(onScreenBulletSript.IsReady()){
				canShoot = true;
			}
		}
	}

	void Shoot(){
		canShoot = false;
		onScreenBullet.transform.localRotation = cannonPivot.localRotation;
		onScreenBullet.transform.position = bulletDeploy.position;
		onScreenBullet.GetComponent<BulletScript> ().Shoot (bulletSpeed);
		audioSource.PlayOneShot (shotFx);
		smokeAnim.SetTrigger ("PLAY");
		cannonAnim.SetTrigger ("SHOOT");
	}

	void CreateBullet(){
		onScreenBullet = Instantiate (bulletPrefab);
		onScreenBulletSript = onScreenBullet.GetComponent<BulletScript> ();
	}
}
