﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	public void show(GameObject item){
		item.SetActive (true);
	}

	public void hide(GameObject item){
		item.SetActive (false);
	}

	public void toggle(GameObject item){
		item.SetActive (!item.activeInHierarchy);
	}

	public void loadScene(string sceneName){
		Time.timeScale = 1;
		SceneManager.LoadScene (sceneName);
	}

	public void quit(){
		Application.Quit ();
	}

	public void pause(){
		if(Time.timeScale == 1){
			Time.timeScale = 0;
		} else if(Time.timeScale == 0){
			Time.timeScale = 1;
		}
	}
}
